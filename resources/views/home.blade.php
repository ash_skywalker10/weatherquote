@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-4">
                <h1>{{ $quote }}</h1>
                <br/>
                <h3>{{ $author }}</h3>
                <h5>{{ $temperature }}&#8451;</h5>
            </div>

            <span style="z-index:50;font-size:0.9em;">
                <img src="http://api.paperquotes.com/static/images/paperquotes.png" height="20" width="20" alt="paperquotes.com">
                <a href="http://paperquotes.com" title="Powered by quotes from paperquotes.com" style="color: #9fcc25; margin-left: 4px; vertical-align: middle;">paperquotes.com
                </a>
            </span>
        </div>
    </div>
</div>
@endsection
