<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $weatherToken = '03e4c3ee1bf954d7a7a8b37ac13de32c';
        $cityToken = '2147714';
        $client = new Client();
        $response = $client->request('GET', "http://api.openweathermap.org/data/2.5/weather?id=$cityToken&APPID=$weatherToken&units=metric");
        $weatherData = json_decode($response->getBody()->getContents(), true);
        $temperature = $weatherData['main']['temp'];

        $categories = [
          'light' => [
            'happiness',
            'funny',
            'humor'
          ],
          'dark' => [
            'inspirational',
            'change',
            'wisdom',
            'trust',
            'respect',
            'sad',
            'hope',
            'great',
            'life',

          ],
        ];

        $selectedCategory = $temperature >= 15 ? 'light' : 'dark';

        $response = $client->request('GET', 'https://talaikis.com/api/quotes/');
        $quoteResponse = json_decode($response->getBody()->getContents(), true);

        $quote = $quoteResponse[0]['quote'];
        $author = $quoteResponse[0]['author'];

        foreach ($quoteResponse as $quoteData) {
          if (in_array($quoteData['cat'], $categories[$selectedCategory])) {
            $quote = $quoteData['quote'];
            $author = $quoteData['author'];
          }
        }

        return view('home', compact('quote','author', 'temperature'));
    }
}
